var express = require('express');
var router = express.Router();
var pool = require('../connections/db');
var indexController = require('../controllers/indexController');
var querystring = require('querystring');
var http = require('http');
var fs = require('fs');
var uuidv4 = require('uuid/v4');
var d3 = require("d3");
var jquerry = require("jquery");
var signInController = require('../controllers/signInController');
/* GET home page. */
/*router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
module.exports = router;*/
router.get('/',function(req, res, next) {
    indexController.get(req, res);
});
router.get('/getTree', async function(req, res, next ) {
    // console.log(result);
    const rows = [];
    const [results] =  await pool.query("SELECT * FROM members");
    console.log(results);
    for (let r of results) {
        let node = {
            key: r['keyid'],
            name: r['name'],
            title:r['title'],
        };
        if (r['parent']) {
            node.parent = r['parent'];
        }
        rows.push(node);
    }
    res.json(rows);
        //connection.release();
});
router.get('/signInController',function(req, res, next) {
    signInController.get(req, res);

});


router.post('/saveTree', async function(req, res, next) {
    console.log(req.body.name);
    var i;
    for (i = 0; i < req.body.nodeDataArray.length; i++) {
        var post = {
            keyid: req.body.nodeDataArray[i].key,
            name: req.body.nodeDataArray[i].name,
            title: req.body.nodeDataArray[i].title,
            parent: req.body.nodeDataArray[i].parent
        };
        var duplicateKey = {
            keyid: req.body.nodeDataArray[i].key
        };
        //if( req.body.nodeDataArray[i].key )
        var sql = 'INSERT INTO members SET ? ON DUPLICATE KEY UPDATE name=VALUES(name) , title=VALUES(title) , parent=VALUES(parent)' ;
        await pool.query(sql, post);

        //var sqlDelete = 'DELETE FROM members WHERE keyid =?'  ;

        //var sqlDelete='DELETE FROM members WHERE ?';
        //await pool.query(sqlDelete,duplicateKey);
        console.log("1 record inserted");
    }
    res.end();
    //connection.release();
});

myDateTime = function () {

    return Date();
};

module.exports = router;