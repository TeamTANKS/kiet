-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.21-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for familytree
DROP DATABASE IF EXISTS `familytree`;
CREATE DATABASE IF NOT EXISTS `familytree` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `familytree`;

-- Dumping structure for table familytree.members
DROP TABLE IF EXISTS `members`;
CREATE TABLE IF NOT EXISTS `members` (
  `keyid` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `parent` int(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`keyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table familytree.members: ~18 rows (approximately)
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` (`keyid`, `name`, `title`, `parent`) VALUES
	('1', 'Tanks', 'Bo gia', NULL),
	('1000', '(new person)', '', 4),
	('1001', '(new person)', '', 3),
	('1002', '(new person)', '', 2),
	('1003', '(new person)', '', 1),
	('1004', '(new person)', '', 3),
	('1005', '(new person)', '', 1001),
	('1006', '(new person)', '', 1000),
	('1007', '(new person)', '', 1005),
	('1008', '(new person)', '', 1004),
	('1009', '(new person)', '', 1003),
	('1010', '(new person)', '', 4),
	('1011', '(new person)', '', 1009),
	('1012', '(new person)', '', 1008),
	('1013', '(new person)', '', 1011),
	('2', 'Thuy', 'hihi', 1),
	('3', 'Nhat', 'haha', 1),
	('4', 'Son', 'hoho', 2);
/*!40000 ALTER TABLE `members` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
